package com.controller;

import com.model.Product;
import com.model.Tables;
import com.model.User;
import com.service.otherservice.ActionService;
import com.service.userservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author tigran
 */

@Component("waiterController")
@Scope(WebApplicationContext.SCOPE_SESSION)
public class WaiterController {

    private final UserService userService;

    private final ActionService actionService;

    @Autowired
    public WaiterController(UserService userService, ActionService actionService) {
        this.userService = userService;
        this.actionService = actionService;
    }


    void addUser() {
        User user = new User();
        userService.saveUser(user);
    }

   void addTable() {
       Tables tables = new Tables();
       actionService.createTable(tables);
    }

    void upDateTable() {
        Tables tables = new Tables();
        actionService.upDateTable(tables);
    }

    void addProduct() {
        Product product = new Product();
        actionService.createProduct(product);
    }


}
