package com.controller;

import com.beans.Login;
import com.model.User;
import com.service.userservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;


/**
 * @author tigran
 */

@Component("userController")
@Scope(WebApplicationContext.SCOPE_SESSION)
public class UserController implements Serializable {

    private final UserService userService;

    private Login login;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    public String saveUser() {
        User user = new User();
        user.setLogin(login.getUsername());
        userService.saveUser(user);
        return "OK";
    }


    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }
}
