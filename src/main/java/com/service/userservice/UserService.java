package com.service.userservice;

import com.model.User;

import java.util.List;

/**
 *
 * @author tigran
 */
public interface UserService {

    void saveUser(User user);

    User findByLogin(String user);

    User findByUserId(long userId);


}
