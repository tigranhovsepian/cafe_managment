package com.service.otherservice;

import com.model.Order;

/**
 *
 * @author tigran
 */
public interface OrderService {

    void createOrder(Order order);

    Order findOrderByOrderNumber(int orderNumber);

}
