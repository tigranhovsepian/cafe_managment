package com.service.otherservice;

import com.model.Product;

/**
 *
 * @author tigran
 */
public interface ProductService {

    Product findByProductName(String productName);

    void createProduct(Product product);
}
