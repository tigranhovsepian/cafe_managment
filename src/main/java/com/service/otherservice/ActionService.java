package com.service.otherservice;

import com.dao.OrderDao;
import com.dao.ProductDao;
import com.dao.TableDao;
import com.model.Order;
import com.model.Product;
import com.model.Tables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author tigran
 */
@Service
public class ActionService implements OrderService, ProductService, TableService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private TableDao tableDao;

    @Override
    public void createOrder(Order order) {
        orderDao.save(order);
    }

    @Override
    public Order findOrderByOrderNumber(int orderNumber) {
        return orderDao.findByOrderNumber(orderNumber);
    }

    @Override
    public void createProduct(Product product) {
        productDao.save(product);
    }

    @Override
    public Product findByProductName(String productName) {
        return productDao.findByProductName(productName);
    }

    @Override
    public void createTable(Tables table) {
        tableDao.save(table);
    }

    @Override
    public Tables findByTableNumber(int tableNumber) {
        return tableDao.findByTableNumber(tableNumber);
    }

    @Override
    public void upDateTable(Tables table) {
        tableDao.updateTable(table.getUser(), table.getTableId());
    }

    @Override
    public Tables findByTableStatus(boolean assigned) {
        return tableDao.findByTableStatus(assigned);
    }

}
