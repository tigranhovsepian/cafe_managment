package com.service.otherservice;

import com.model.Tables;
import java.util.List;

/**
 *
 * @author tigran
 */
public interface TableService {

    void createTable(Tables table);

    void upDateTable(Tables table);

    Tables findByTableNumber(int tableNumber);

    Tables findByTableStatus(boolean assigned);

}
