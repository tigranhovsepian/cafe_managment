package com.dao;

import com.model.Order;
import com.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author tigran
 */


public interface OrderDao extends JpaRepository<Order, Long> {

    @Query("select b from Order b where b.orderNumber = :orderNumber")
    Order findByOrderNumber(@Param("orderNumber") int orderNumber);


}
