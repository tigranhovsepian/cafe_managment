package com.dao;


import com.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author tigran
 */
public interface ProductDao extends JpaRepository<Product, Long> {

    @Query("select b from Product b where b.productName = :productName")
    Product findByProductName(@Param("productName") String productName);

}
