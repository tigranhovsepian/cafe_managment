package com.dao;

import com.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author tigran
 */
public interface UserDao extends JpaRepository <User, Long> {

    @Query("select b from User b where b.login = :login")
    User findByLogin(@Param("login") String login);

    @Query("select b from User b where b.id = :userId")
    User findByUserId(@Param("userId") long userId);

}
