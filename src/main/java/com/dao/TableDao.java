package com.dao;

import com.model.Tables;
import com.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 *
 * @author tigran
 */
public interface TableDao extends JpaRepository <Tables, Long> {

    @Modifying
    @Query("update Tables u set u.user = ?1 where u.tableId = ?2")
    void updateTable(User user, long tableId);

    @Query("select b from Tables b where b.tableNumber = :tableNumber")
    Tables findByTableNumber(@Param("tableNumber") int tableNumber);

    @Query("select b from Tables b where b.assigned = :assigned")
    Tables findByTableStatus(@Param("assigned") boolean assigned);

    List<Tables> findAll();

}
