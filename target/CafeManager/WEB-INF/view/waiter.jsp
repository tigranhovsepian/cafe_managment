<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:a="http://www.custom.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:rich="http://richfaces.org/rich">

<ui:composition template="/pages/templates/mainPage.jsp">
    <ui:define name="body">
        <a4j:keepAlive beanName="waiterController"/>
        <br/>

        <rich:panel style="width:99%;">
				<span>
					<table border="1" cellspacing="3" cellpadding="3" width="100%" style="border-collapse:collapse;" bordercolor="#dbdadc">
						<tr>
							<td width="50%">
								<h:outputText value="UserName"/>
							</td>
							<td>
								<h:inputText id="userName" value="#{waiterController.userName}" readonly="true" style="width:99%;" />
							</td>
						</tr>

                        <tr>
							<td width="50%">
								<h:outputText value="password"/>
							</td>
							<td>
								<h:inputText id="password" value="#{waiterController.password}" readonly="true" style="width:99%;" />
							</td>
						</tr>

                        <tr>
							<td width="50%">
								<h:outputText value="role"/>
							</td>
							<td>
								<h:inputText id="role" value="#{waiterController.role}" readonly="true" style="width:99%;" />
							</td>
						</tr>

					</table>
					 <h:commandButton value="CreateUser" style="cursor:pointer;" action="#{waiterController.addUser}" />
				</span>
        </rich:panel>
    </ui:define>
</ui:composition>

</html>